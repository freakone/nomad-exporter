package main

import (
	"fmt"
	"regexp"

	nomad "github.com/hashicorp/nomad/api"
	"github.com/sirupsen/logrus"
)

// Scans the AllocationListStub and check for duplicates. It replaces the index with alloc id if it fins it
func DeduplicateStubList(allocations []*nomad.AllocationListStub) {
	names := make(map[string]string)
	r := regexp.MustCompile(`(.*)\[([0-9]+)\]$`)

	for _, allocStub := range allocations {

		if allocStub.ClientStatus != "running" {
			continue
		}

		allocNameWithNodeID := fmt.Sprintf("%s@%s", allocStub.Name, allocStub.NodeID)

		if cachedAlloc, exists := names[allocNameWithNodeID]; exists && allocStub.ID != cachedAlloc {
			newAllocName := allocStub.Name

			if !r.MatchString(allocStub.Name) {
				newAllocName = fmt.Sprintf("%s[0]", newAllocName)
			}

			newAllocName = r.ReplaceAllString(newAllocName, fmt.Sprintf("$1[%s]", allocStub.ID))
			logrus.Debugf("Alloc '%s' renamed to '%s' (alloc with id '%s' has the same name).", allocStub.Name, newAllocName, cachedAlloc)

			allocStub.Name = newAllocName
			allocationDuplicatedName.Inc()
		}
		names[allocNameWithNodeID] = allocStub.ID
	}
}
