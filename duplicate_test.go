package main

import (
	"fmt"
	"testing"

	nomad "github.com/hashicorp/nomad/api"
)

func TestDeduplicateStubListAgainstDuplicates(t *testing.T) {
	allocList := []*nomad.AllocationListStub{
		&nomad.AllocationListStub{Name: "some.metrics", ID: "52acb440", NodeID: "node1337-txdq", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "some.metrics", ID: "52acb440", NodeID: "node1337-txdq", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "some.metrics", ID: "85a510ba", NodeID: "node1337-txdq", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "some.metrics", ID: "88a210bb", NodeID: "node1337-trwa", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "nomad.nomad[0]", ID: "10f40613", NodeID: "node1337-txdq", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "nomad.nomad[0]", ID: "ba70cf1d", NodeID: "node1337-txdq", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "nomad.runtime[0]", ID: "93bdb9ad", NodeID: "node1337-txdq", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "nomad.memberlist[0]", ID: "553c715e", NodeID: "node1337-txdq", ClientStatus: "running"},
	}

	DeduplicateStubList(allocList)

	names := make(map[string]string)
	for _, alloc := range allocList {
		allocNameWithNodeID := fmt.Sprintf("%s@%s", alloc.Name, alloc.NodeID)

		if cachedAllocID, exists := names[allocNameWithNodeID]; exists && alloc.ID != cachedAllocID {
			t.Errorf("The alloc with id %s and %s share the same names %s on the same node", cachedAllocID, alloc.ID, alloc.Name)
		}
		names[allocNameWithNodeID] = alloc.ID
	}
}

func TestDeduplicateStubListResultNames(t *testing.T) {
	allocList := []*nomad.AllocationListStub{
		&nomad.AllocationListStub{Name: "some.metrics", ID: "52acb440", NodeID: "node1337-txdq", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "some.metrics", ID: "52acb440", NodeID: "node1337-txdq", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "some.metrics", ID: "85a510ba", NodeID: "node1337-txdq", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "some.metrics", ID: "88a210bb", NodeID: "node1337-trwa", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "nomad.nomad[0]", ID: "10f40613", NodeID: "node1337-txdq", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "nomad.nomad[0]", ID: "ba70cf1d", NodeID: "node1337-txdq", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "nomad.runtime[0]", ID: "93bdb9ad", NodeID: "node1337-txdq", ClientStatus: "running"},
		&nomad.AllocationListStub{Name: "nomad.memberlist[0]", ID: "553c715e", NodeID: "node1337-txdq", ClientStatus: "running"},
	}

	resultNames := []string{"some.metrics",
		"some.metrics",
		"some.metrics[85a510ba]",
		"some.metrics",
		"nomad.nomad[0]",
		"nomad.nomad[ba70cf1d]",
		"nomad.runtime[0]",
		"nomad.memberlist[0]"}

	DeduplicateStubList(allocList)

	for idx, alloc := range allocList {
		if alloc.Name != resultNames[idx] {
			t.Errorf("The alloc with original name %s and id %s should be renamed to: %s", alloc.Name, alloc.ID, resultNames[idx])
		}
	}
}
